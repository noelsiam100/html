from django.db import models
from django.db.models.expressions import F

# Create your models here.
class students(models.Model):
    std_id=models.IntegerField(null=False,blank=False)
    std_name=models.CharField(max_length=100,null=False,blank=False)
    std_DOB=models.DateField(max_length=10,null=False,blank=False)
    std_gender=models.CharField(max_length=50,null=False,blank=False)
    std_dateofjoin=models.DateField()
    English=models.IntegerField()
    Maths=models.IntegerField()
    Software_Eng=models.IntegerField()
    Practial=models.IntegerField()
    
    @property
    def Average_marks(self):
         return (self.English+self.Maths+self.Software_Eng+self.Practial)/4
       


def __str__(self):
    return self.std_name    