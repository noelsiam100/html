from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import  serializers, status
from rest_framework.decorators import api_view, permission_classes, renderer_classes 
from . serializers import studentsSerializer
from . models import students
from rest_framework.permissions import IsAuthenticated


# Create your views here.
@api_view(['GET'])
def studentsOverview(request):
    permission_classes=(IsAuthenticated,)
    students_urls={
        'list':'/students-list/',
        'Detials View':'/studenst-detials/',
        'create':'/students-create/',
        'update':'/students-update/',
    }

    return Response(students_urls); 

@api_view(['GET'])
def ShowAll(request):
    student=students.objects.all()
    serializer=studentsSerializer(student, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def marks(request,pk):
    student=students.objects.get(students.English,students.Maths,students.Software_Eng,students.Practial)
    serializer=studentsSerializer(student,many=False)
    return response(serializer.data)
