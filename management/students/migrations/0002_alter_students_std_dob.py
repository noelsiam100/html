# Generated by Django 3.2.9 on 2021-11-29 07:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='students',
            name='std_DOB',
            field=models.DateField(max_length=10),
        ),
    ]
